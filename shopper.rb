#!/usr/bin/env ruby
# 
Dir[File.dirname(__FILE__) + '/lib/*'].each {|file| require file }

class Shopper
  attr_reader :products
  def initialize
    @rules = load_rules
    shop
  end

  def shop
    co = Checkout.new(@rules)

    i1 = Item.new(001, 'Travel Card Holder', 9.25)
    i2 = Item.new(002, 'Personalised cufflinks', 45.00)
    i3 = Item.new(003, 'Kids T-shirt', 19.95)

    co.scan(i1)
    co.scan(i2)
    co.scan(i3)

    p price = co.total
  end

  def load_rules
    [
      TotalDiscountRule.new({ discount_treshold: 60, discounted_percents: 10 }),
      PerItemsDiscountRule.new({ item_code: 001, discounted_quantity: 2, discounted_price: 8.5 })
    ]
  end
end

Shopper.new