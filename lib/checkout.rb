require_relative 'item'

class Checkout
  attr_reader :items

  def initialize rules=[]
    @rules = rules

    @total_price = 0
    @items = []
  end

  def scan item
    @items << item
  end

  def total
    total = discount_items
    total = discount_total(total)
    round_to_cents(total)
  end

  private
  # TODO 
  # provide solution when one item has mulitple rules
  # apply all, apply one, ore apply specific?
  # 
  # Implemented: Only first rule in not specified order is applied
  # 
  def discount_items
    total_price = 0

    grouped_items.each do |item_code, item_group|

      rule = rules_for_item(item_code).first
      rule.evaluate(item_group) unless rule.nil?
      
      item_group.each do |item|
        total_price += rule.nil? ? item.price : rule.apply(item)
      end
    end

    total_price
  end

  # TODO
  # Specify policy how to treat number multiple rules for total.
  # 
  # Implemented: All rules are applied
  # 
  def discount_total total
    if rules_for_total.count.zero?
      total
    else
      tmp_total = total

      rules_for_total.each do |rule|
        tmp_total -= rule.apply(total)
      end

      tmp_total
    end
  end

  def grouped_items
    @items.group_by{|item| item.code }
  end

  def rules_per_item
    @rules_per_item ||= @rules.select{ |rule| rule.type?('item') }
  end

  def rules_for_item item_code
    rules_per_item.select{ |rule| rule.applies_to?(item_code) }
  end

  def rules_for_total
    @rules_for_total ||= @rules.select{ |rule| rule.type?('total') }
  end

  def round_to_cents total
    total.round(2)
  end
end