require_relative 'discount_rule'

class TotalDiscountRule < DiscountRule

  TYPE = 'total'

  def initialize attrs
    @treshold_amount = attrs[:discount_treshold]
    @discounted_percents = attrs[:discounted_percents]
  end

  def apply(total)
    discounted?(total) ? (total - discounted_amount(total)) : 0
  end

  def discounted_amount total
    percents = (total.to_f / @discounted_percents.to_f)
    total - percents
  end

  def discounted? total
    total >= @treshold_amount
  end

  def type? type_name
    type_name == TYPE
  end
end