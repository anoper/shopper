class DiscountRule
  def apply item
    raise 'Missing rule implementation, or trying to apply abstract rule.'
  end
end