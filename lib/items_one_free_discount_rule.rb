require_relative 'discount_rule'

class FreeItemDiscountRule < DiscountRule

  TYPE = 'item'

  def initialize attrs
    @free_item_treshold = attrs[:free_item_treshold]
    @number_of_free_items = attrs[:number_of_free_items]
    @item_code = attrs[:item_code]
    @items_in_basket = 0
    @applied_items_count = 0
  end

  def evaluate item_group
    @items_in_basket = item_group.count
  end

  def apply item
    raise "Invalid rule for item #{item.code}, expected item #{@item_code}" unless applies_to?(item.code)
    @applied_items_count += 1
    price = (@items_in_basket % @free_item_treshold == 0) ? item.price - (item.price * @number_of_free_items) : item.price
  end

  # TODO extract to parent class
  def applies_to? item_id
    item_id == @item_code
  end
end