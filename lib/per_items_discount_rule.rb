require_relative 'discount_rule'

class PerItemsDiscountRule < DiscountRule
  attr_reader :item_code, :discounted_quantity, :discounted_price

  TYPE = 'item'

  def initialize attrs
    @item_code = attrs[:item_code]
    @discounted_quantity = attrs[:discounted_quantity]
    @discounted_price = attrs[:discounted_price]

    @applied_count = 0
    @applicable = false
  end

  def evaluate(item_group)
    @applicable = true if (item_group.count >= @discounted_quantity)
  end

  def apply item
    raise "Invalid rule for item #{item.code}, expected item #{@item_code}" unless applies_to?(item.code)
    @applied_count += 1

    price = discounted? ? discounted_price : item.price
  end

  def discounted?
    @applicable
  end

  def applies_to? item_id
    item_id == @item_code
  end

  def type? type_name
    type_name == TYPE
  end
end