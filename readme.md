# Shopper

## Instructions

- operate from root directory

run example application using command-line

    ./shopper.rb

run test

    rake test

## Note

Sample data are tested in CheckoutTest -> TEST CASE 1, 2 and 3.