require 'test/unit'

require_relative '../lib/checkout'
require_relative '../lib/per_items_discount_rule'
require_relative '../lib/total_discount_rule'
require_relative '../lib/item'

class CheckoutChannlengeTest < Test::Unit::TestCase
  def setup
    @rule_item = TotalDiscountRule.new({ discount_treshold: 60, discounted_percents: 10 })
    @rule_total = PerItemsDiscountRule.new({ item_code: 001, discounted_quantity: 2, discounted_price: 8.5 })
    
    @co = Checkout.new([@rule_item, @rule_total])
    
    @i1 = Item.new(001, 'Travel Card Holder', 9.25)
    @i2 = Item.new(002, 'Personalised cufflinks', 45.00)
    @i3 = Item.new(003, 'Kids T-shirt', 19.95)
  end

  #
  # TEST CASE 1
  #
  # SAMPLE 1
  # Basket: 001,002,003
  # Total price expected: £66.78
  #
  def test_mixed_discount_rules
    @co.scan(@i1)
    @co.scan(@i2)
    @co.scan(@i3)

    assert(@co.total == 66.78)
  end

  #
  # TEST CASE 2
  #
  # SAMPLE 2
  # Basket: 001,003,001
  # Total price expected: £36.95
  #
  def test_mixed_rules_returns_double_item_discount
    @co.scan(@i1)
    @co.scan(@i3)
    @co.scan(@i1)
    assert(@co.total == 36.95)
  end

  #
  # TEST CASE 3
  #
  # SAMPLE 3
  # Basket: 001,002,001,003
  # Total price expected: £73.76
  #
  def test_mixed_rules_returns_total_discount
    @co.scan(@i1)
    @co.scan(@i2)
    @co.scan(@i1)
    @co.scan(@i3)
    assert(@co.total == 73.76)
  end
end

class CheckoutTest < Test::Unit::TestCase
  def test_checkout_with_no_discount
    @checkout = Checkout.new
    item = Item.new(001, 'Mars', 0.8)
    @checkout.scan(item)
    @checkout.scan(item)

    assert(@checkout.total == 1.6)
  end

  def test_checkout_with_discount_per_item_not_discounted
    @rule = PerItemsDiscountRule.new({ item_code: 001, discounted_quantity: 2, discounted_price: 8.5 })
    @item = Item.new(001, 'Travel Card Holder', 9.25)

    checkout = Checkout.new([@rule])
    checkout.scan(@item)
    assert(checkout.total == 9.25)
  end

  def test_checkout_with_discount_per_item_discounted
    discounted_price = 8.5
    rule = PerItemsDiscountRule.new({ item_code: 001, discounted_quantity: 2, discounted_price: discounted_price })
    item = Item.new(001, 'Travel Card Holder', 9.25)

    checkout = Checkout.new([rule])
    checkout.scan(item)
    checkout.scan(item)
    assert(checkout.total == 17.0)
  end

  def test_checkout_with_discount_on_total_price_not_discounted
    rule = TotalDiscountRule.new({ discount_treshold: 100, discounted_percents: 10 })
    item = Item.new(001, 'Travel Card Holder', 99.99)

    checkout = Checkout.new([rule])
    checkout.scan(item)
    assert(checkout.total == 99.99)
  end

  def test_checkout_with_discount_on_total_price_discounted
    rule = TotalDiscountRule.new({ discount_treshold: 100, discounted_percents: 10 })
    item = Item.new(001, 'Travel Card Holder', 100.00)

    checkout = Checkout.new([rule])
    checkout.scan(item)
    assert(checkout.total == 90.00)
  end

  def test_checkout_with_2_total_discounts
    rule = TotalDiscountRule.new({ discount_treshold: 100, discounted_percents: 10 })
    rule1 = TotalDiscountRule.new({ discount_treshold: 100, discounted_percents: 10 })
    item = Item.new(001, 'Travel Card Holder', 100.00)

    checkout = Checkout.new([rule, rule1])
    checkout.scan(item)
    assert(checkout.total == 80.00)
  end
end