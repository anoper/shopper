require 'test/unit'

require_relative '../lib/per_items_discount_rule'
require_relative '../lib/item'

class PerItemsDiscountTest < Test::Unit::TestCase
  def setup
    @discounted_price = 8.5
    @rule = PerItemsDiscountRule.new({ item_code: 001, discounted_quantity: 2, discounted_price: @discounted_price })
  end

  def test_not_discount_for_one_item
    item = Item.new(001, 'Travel Card Holder', 9.25)
    assert(@rule.apply(item) == item.price)
  end

  def test_discounted_second_item
    item = Item.new(001, 'Travel Card Holder', 9.25)
    items = [item, item]
    @rule.evaluate(items)
    assert(@rule.apply(item) == @discounted_price)
  end

  def test_discounted_third_item
    item = Item.new(001, 'Travel Card Holder', 9.25)
    items = [item, item, item]
    @rule.evaluate(items)
    assert(@rule.apply(item) == @discounted_price)
  end

  def test_raise_exception_for_invalid_item
    item = Item.new(002, 'Mars', 9.25)
    assert_raise RuntimeError do
      @rule.apply(item)
    end
  end
end