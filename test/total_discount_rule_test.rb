require 'test/unit'

require_relative '../lib/total_discount_rule'

class TotalDiscountRuleTest < Test::Unit::TestCase
  def setup
    @rule = TotalDiscountRule.new({ discount_treshold: 100, discounted_percents: 10 })
  end

  def test_no_discount_for_less_then_trashold
    assert(@rule.apply(99) == 0)
  end

  def test_discount_trashold
    @rule.apply(100)
    assert(@rule.apply(100) == 10)
  end

  def test_discount_more_then_trashold
    assert(@rule.apply(200) == 20)
  end
end