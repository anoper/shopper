require 'test/unit'

require_relative '../lib/items_one_free_discount_rule'
require_relative '../lib/item'

class FreeItemDiscountRuleTest < Test::Unit::TestCase
  def setup
    @discounted_price = 8.5
    @item_price = 9.5
    @rule = FreeItemDiscountRule.new({ item_code: 001, free_item_treshold: 3, number_of_free_items: 1})
    item_price = @item_price
  end

  def test_one_item_normal_price
    item = Item.new(001, 'Travel Card Holder', @item_price)
    @rule.evaluate([item])
    assert(@rule.apply(item) == @item_price)
  end

  def test_second_item_normal_price
    item = Item.new(001, 'Travel Card Holder', @item_price)
    @rule.evaluate([item, item])
    @rule.apply(item)
    @rule.apply(item)
    assert(@rule.apply(item) == @item_price)
  end

  def test_third_item_is_free
    item = Item.new(001, 'Travel Card Holder', @item_price)
    @rule.evaluate([item, item, item])
    @rule.apply(item)
    @rule.apply(item)
    assert(@rule.apply(item) == 0)
  end
end